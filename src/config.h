#ifndef CONFIG_H_KO8K9RVM
#define CONFIG_H_KO8K9RVM

#include <stdbool.h>

struct config_t
{
    char path_rfid[512];
};

bool config_init( struct config_t* const cfg, const char* const config_path );

#endif /* end of include guard: CONFIG_H_KO8K9RVM */
