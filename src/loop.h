#ifndef LOOP_H_9NIEX2GQ
#define LOOP_H_9NIEX2GQ

typedef void (*LOOP_CALLBACK)(void *);
typedef void (*TIMER_CALLBACK)(int, void *);

int loop_add_fd(int fd, LOOP_CALLBACK callback, void * data);
int loop_add_gpio_fd(int fd, LOOP_CALLBACK callback, void * data);
int loop_add_timer(int msec, TIMER_CALLBACK callback, void * data);
void loop_clear_timer(int timer_id);
void loop_run(void);

#endif /* end of include guard: LOOP_H_9NIEX2GQ */
