#include "loop.h"

#include <poll.h>
#include <sys/timerfd.h>
#include <unistd.h>
#include <stdint.h>

#include <stdio.h>
#include <errno.h>
#include <string.h>

struct LOOP_DATA
{
    LOOP_CALLBACK callback;
    void * data;
};

struct TIMER_DATA
{
    TIMER_CALLBACK callback;
    void * data;
    int fd;
};

#define MAX_FDS 256
#define MAX_TIMERS 16

static struct pollfd loop_fds[MAX_FDS];
static struct LOOP_DATA loop_data[MAX_FDS];
static struct TIMER_DATA loop_timers[MAX_TIMERS];

static int loop_num_fds = 0;
static int loop_num_timers = 0;

void loop_run(void)
{
    for (;;)
    {
        if (poll(loop_fds, loop_num_fds, -1) > 0)
        {
            int i;
            for (i = 0; i < MAX_FDS; i++)
            {
                if (loop_fds[i].revents != 0)
                {
                    if (loop_data[i].callback)
                        loop_data[i].callback(loop_data[i].data);
                    loop_fds[i].revents = 0;
                }
            }
        }
    }
}

static int loop_add_fd_private(int fd, int events, LOOP_CALLBACK callback, void * data)
{
    if (loop_num_fds + 1 >= MAX_FDS)
        return 0;

    loop_fds[loop_num_fds].fd = fd;
    loop_fds[loop_num_fds].events = events;
    loop_fds[loop_num_fds].revents = 0;
    loop_data[loop_num_fds].callback = callback;
    loop_data[loop_num_fds].data = data;
    loop_num_fds++;

    return 1;
}

int loop_add_fd(int fd, LOOP_CALLBACK callback, void * data)
{
    return loop_add_fd_private(fd, POLLIN, callback, data);
}

int loop_add_gpio_fd(int fd, LOOP_CALLBACK callback, void * data)
{
    return loop_add_fd_private(fd, POLLPRI | POLLERR, callback, data);
}

static void timer_callback(void * data)
{
    int timer_id = (intptr_t) data;
    if (loop_timers[timer_id].callback)
        loop_timers[timer_id].callback(timer_id, loop_timers[timer_id].data);
}

int loop_add_timer(int msec, TIMER_CALLBACK callback, void * data)
{
    if (loop_num_fds + 1 >= MAX_TIMERS)
        return 0;

    int fd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
    if (fd == -1)
        return -1;

    struct itimerspec its;
    memset(&its, '\0', sizeof(its));
    its.it_value.tv_sec = its.it_interval.tv_sec = msec / 1000;
    its.it_value.tv_nsec = its.it_interval.tv_nsec = (msec % 1000) * 1000000;
    if (timerfd_settime(fd, 0, &its, NULL) != 0)
        goto err;

    // Incrementing before use so that timer IDs start from 0
    // (since 0 is already used for error return)
    loop_num_timers++;
    int timer_id = loop_num_timers;
    if (!loop_add_fd(fd, &timer_callback, (void *) (intptr_t) timer_id))
        goto err;

    loop_timers[timer_id].callback = callback;
    loop_timers[timer_id].data = data;
    loop_timers[timer_id].fd = fd;
    return timer_id;

    err:
        close(fd);
        return -1;
}

void loop_clear_timer(int timer_id)
{
    uint64_t tmp;
    read(loop_timers[timer_id].fd, &tmp, sizeof(uint64_t));
}

