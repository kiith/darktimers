#include "config.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>

bool config_init( struct config_t* const cfg, const char* const config_path )
{
    FILE* file = fopen(config_path, "rb");
    if (0 == file)
    {
        fprintf(stderr, "Failed to open '%s': %s\n", config_path, strerror(errno));
        return false;
    }
    memset(cfg, 0, sizeof(*cfg));

    char key[512];
    char value[512];

    int num_read;
    do
    {
        num_read = fscanf(file, "%511s %511s\n", key, value);
        /* not a config line */
        if(2 != num_read)
        {
            continue;
        }

        if(0 == strncmp(key, "rfid_input_path", 511))
        {
            strncpy(cfg->path_rfid, value, 511);
            printf("%s: %s\n", key, cfg->path_rfid);
        }
    } while( EOF != num_read );

    if (ferror(file))
    {
        fprintf(stderr, "Error reading '%s': %s", config_path, strerror(errno));
    }
    fclose(file);
    return true;
}


