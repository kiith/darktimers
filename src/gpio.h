#ifndef GPIO_H_P0FGUIMS
#define GPIO_H_P0FGUIMS

enum GPIO_Direction
{
   GPIO_IN,
   GPIO_OUT
};

enum GPIO_Value
{
    GPIO_UNKNOWN = -1,
    GPIO_LOW,
    GPIO_HIGH
};

typedef struct GPIO GPIO;

GPIO * gpio_open(int num, enum GPIO_Direction dir);
int gpio_write(GPIO * gpio, enum GPIO_Value val);
enum GPIO_Value gpio_read(GPIO * gpio);
int gpio_get_fd(GPIO * gpio);
int gpio_close(GPIO * gpio);

#endif /* end of include guard: GPIO_H_P0FGUIMS */
