#include <stdio.h>
#include <time.h>

#include "config.h"
#include "loop.h"
#include "machineid.h"
#include "rfid.h"

void timer_callback(int timer_id, void * data)
{
    puts("Hello!\n");
	loop_clear_timer(timer_id);
}

void generate_out_path( char* const path, const size_t length )
{
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    int offset = machine_id(path, length);
    if(0 > offset)
    {
        // in case on error try working without the mac address
        offset = 0;
    }
    strftime(path + offset, length - 1 - offset, "_%d_%m_%Y_%H:%M.csv", t);
}
int main(int argc, char** argv)
{
    /* default path in home, overridable by first CLI arg */
    const char* config_path = "darktimersrc";
    if (argc >= 2)
    {
        config_path=argv[1];
    }

    struct config_t config;
    if (!config_init(&config, config_path))
    {
        return 1;
    }

    char out_path[256];
    generate_out_path(out_path, sizeof(out_path));
    fprintf(stderr, "out_path: '%s'\n", out_path);

    struct rfid_data_t rfid;
    if(!rfid_init(&rfid, config.path_rfid, out_path))
    {
        return 2;
    }

    loop_add_timer(1000, &timer_callback, NULL);
    loop_run();

    rfid_free(&rfid);
    return 0;
}
